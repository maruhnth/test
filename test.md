# t-Test bei unabhängigen Stichproben


## __Formel für ungleiche Varianzentt:__

→ auch für den Fall gleicher Varianzen anwendbar


```math
t_{df} = \frac{\overline{x_{A}}-{\overline{x_{A}}}} {\sqrt{\frac{\hat{\sigma}_A^2}{n_A} + \frac{\hat{\sigma}_B^2}{n_B}}}
```

mit

```math
\hat{\sigma}_A^2 = \frac{\sum_{i=1}^{n_A} (x_i - \overline{x_A})^2} {n_{A}-1}
```

## __Welch-Prozedur:__
→ Korrektur der Freiheitsgrade bei ungleichen Varianzen
→ Bühner & Ziegler empfehlen diese Korrektur unabhängig von den Varianzen


## __Alpha-Adjustierung:__

→ α-Fehlerinflation: Gefahr des α-Fehlers kumuliert sich bei mehreren Einzelvergleichen

→ Die Wahrscheinlichkeit bei k unabhängigen Einzelvergleichen einen oder mehrere α-Fehler zu begehen beträgt: $`1-(1-α)^k`$

→ α-Fehlerkorrektur ist für unabhängige Signifikanztests exakt und führt bei abhängigen Tests zu konservativen Entscheidungen
```math
a_{adj} = \frac{a}{k}
```
wobei k= Anzahl der durchgeführten Tests


# Datensätze

[test](//datenb/Projekte$/2-Externe_Projekte/P15-0827_ Kärcher_Brand_Performance/Protokolle/2. Auswertung/0. Datensätze)


[I'm an inline-style link](//192.168.208.5/Projekte$/2-Externe_Projekte/P15-0504_Porsche_Brand_Tracking/Protokolle/50_Auswertung/2018/MaMo-2018_Datenmodell_MaMo_190703.xlsx)

[I'm an inline-style link](www.google.comx)


[I'm an inline-style link](//192.168.208.5/Projekte$/2-Externe_Projekte/P15-0504_Porsche_Brand_Tracking/Protokolle/50_Auswertung/2018/MaMo